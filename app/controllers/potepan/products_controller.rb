class Potepan::ProductsController < ApplicationController
  NUMBER_OF_RELATED_PRODUCTS_LIMIT = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.sample(NUMBER_OF_RELATED_PRODUCTS_LIMIT)
  end
end
