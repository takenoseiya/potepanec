require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_productsメソッドの検証" do
    let(:taxon_a) { create(:taxon, name: "bags") }
    let(:taxon_b) { create(:taxon, name: "ruby on rails") }
    let(:other) { create(:taxon, name: "ruby") }
    let!(:product) { create(:product, taxons: [taxon_a, taxon_b]) }
    let!(:some_products) { create(:product, taxons: [taxon_a, taxon_b]) }
    let!(:other_product) { create(:product, taxons: [other]) }

    it "関連製品を含める" do
      expect(product.related_products).to include some_products
    end

    it "関連しない製品を含まない" do
      expect(product.related_products).not_to include(other_product)
    end

    it "関連製品に表示製品を含めない" do
      expect(product.related_products).not_to include product
    end
  end
end
