require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "カテゴリーページへのアクセス" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "HTTP レスポンスステータスが200で成功していること" do
      expect(response.status).to eq(200)
    end

    it "showテンプレートを使用してレンダリングしている" do
      expect(response).to render_template(:show)
    end

    it 'taxonomies の値が正しいこと' do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it 'taxon の値が正しいこと' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'products の値が正しいこと' do
      expect(assigns(:products)).to match_array(taxon.products)
    end
  end
end
