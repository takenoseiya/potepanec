require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "商品詳細ページへのアクセス" do
    let(:taxon_a) { create(:taxon, name: "bags") }
    let(:taxon_b) { create(:taxon, name: "ruby on rails") }
    let(:other) { create(:taxon, name: "ruby") }
    let!(:product) { create(:product, taxons: [taxon_a, taxon_b]) }
    let!(:some_products) { create_list(:product, 6, taxons: [taxon_a, taxon_b]) }
    let!(:other_product) { create(:product, taxons: [other]) }

    before do
      get :show, params: { id: product.id }
    end

    it "HTTP レスポンスステータスが200で成功していること" do
      expect(response.status).to eq(200)
    end

    it "showテンプレートを使用してレンダリングしている" do
      expect(response).to render_template(:show)
    end

    it 'product の値が正しいか' do
      expect(assigns(:product)).to eq product
    end

    it "related_productsが４つ存在すること" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
