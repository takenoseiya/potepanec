require 'rails_helper'

RSpec.describe "商品詳細ページ表示", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon_a) { create(:taxon, name: "bags", taxonomy: taxonomy) }
  let(:taxon_b) { create(:taxon, name: "ruby on rails", taxonomy: taxonomy) }
  let(:other) { create(:taxon, name: "ruby", taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon_a, taxon_b]) }
  let!(:some_products) { create_list(:product, 4, taxons: [taxon_a, taxon_b]) }
  let!(:other_product) { create(:product, taxons: [other]) }

  before do
    visit potepan_product_path(product.id)
  end

  describe "商品の詳細ページ表示" do
    it "商品名、画像、価格が表示される" do
      within(:css, '.media-body') do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
      end
      within(:css, '.productSlider') do
        expect(page).to have_content product.display_image.attachment_file_name
      end
    end

    it "「一覧ページへ戻る」をクリックするとカテゴリー一覧ページへジャンプする" do
      within(:css, '.media-body') do
        expect(page).to have_link "一覧ページへ戻る"
        click_link "一覧ページへ戻る", href: potepan_category_path(taxon_a.id)
      end
    end

    it "関連商品の商品名、価格、画像が4つ表示される" do
      within(:css, '.productsContent') do
        expect(page).to have_content some_products.first.name
        expect(page).to have_content some_products.first.display_price
        expect(page).to have_content some_products.first.display_image.attachment_file_name
        expect(page).to have_selector ".productBox", count: 4
      end
    end

    it " 関連商品の商品名をクリックすると詳細ページへジャンプする" do
      within(:css, '.productsContent') do
        expect(page).to have_link some_products.first.name
        click_link some_products.first.name, href: potepan_product_path(some_products.first.id)
      end
    end

    it "関連しない商品の商品名が表示されないこと" do
      within(:css, '.productsContent') do
        expect(page).not_to have_content other_product.name
      end
    end
  end
end
