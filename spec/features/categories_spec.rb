require 'rails_helper'

RSpec.describe "カテゴリーページ表示", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other) { create(:taxon, name: "ruby", taxonomy: taxonomy) }
  let!(:product_a) { create(:product, name: "ruby on rails tote", taxons: [taxon]) }
  let!(:product_b) { create(:product, name: "ruby on rails T-shirts", taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
    click_link taxonomy.name
  end

  describe "カテゴリーの一覧表示" do
    it "カテゴリーが一覧表示される" do
      within(:css, '.side-nav') do
        expect(page).to have_content "#{taxon.name}(#{taxon.active_products.count})"
      end
    end

    it "選択したカテゴリーの商品一覧ページのリンクが表示される" do
      within(:css, '.side-nav') do
        expect(page).to have_link taxon.name
        click_link taxon.name, href: potepan_category_path(taxon.id)
      end
    end
  end

  describe "商品の一覧表示" do
    before do
      click_link taxon.name
    end

    it "選択したカテゴリーの商品が一覧表示される" do
      within(:css, '.products_content') do
        expect(page).to have_content product_a.name
        expect(page).to have_content product_a.display_price
        expect(page).to have_content product_a.display_image.attachment_file_name
        expect(page).to have_content product_b.name
        expect(page).to have_content product_b.display_price
        expect(page).to have_content product_b.display_image.attachment_file_name
      end
    end

    it "該当商品の詳細ページのリンクが表示される" do
      within(:css, '.products_content') do
        expect(page).to have_link product_a.name
        click_link product_a.name, href: potepan_product_path(product_a.id)
      end
    end
  end

  describe "選択したカテゴリー外の商品の検証" do
    before do
      visit potepan_category_path(other.id)
    end

    it "商品が表示されない" do
      within(:css, '.products_content') do
        expect(page).not_to have_content product_a.name
        expect(page).not_to have_link product_a.name
      end
    end
  end
end
