require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  describe "full_titleのテスト" do
    subject { full_title(page_title) }

    context "page_titleに空文字が渡された場合" do
      let(:page_title) { "" }

      it "「BIGBAG Store」と表示される" do
        expect(full_title(page_title)).to eq("BIGBAG Store")
      end
    end

    context "page_titleにnilが渡された場合" do
      let(:page_title) { nil }

      it "「BIGBAG Store」と表示される" do
        expect(full_title(page_title)).to eq("BIGBAG Store")
      end
    end

    context "page_titleに引数(Ruby on Rails Tote)が渡された場合" do
      let(:page_title) { "Ruby on Rails Tote" }

      it "「Ruby on Rails Tote - BIGBAG Store」と表示される" do
        expect(full_title(page_title)).to eq("Ruby on Rails Tote - BIGBAG Store")
      end
    end
  end
end
